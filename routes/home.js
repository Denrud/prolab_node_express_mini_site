const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    const content = {
        title: 'design agency',
        description: 'thi is description'
    }

    const db = [
        {id: '1', title: 'post-1', content: 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Provident, qui illum? Nam libero at ut veritatis debitis nihil est et cupiditate facilis, nisi dicta. Mollitia iure deserunt maxime repellat modi?'},
        {id: '2', title: 'post-2', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde sapiente similique expedita possimus distinctio iusto est voluptates provident recusandae ex delectus accusamus nesciunt ratione ad veritatis repellat, explicabo quaerat consectetur.'},
        {id: '3', title: 'post-3', content: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit, aliquid explicabo impedit itaque totam porro sed commodi officia. Deserunt iure maiores consequatur numquam illo commodi pariatur culpa modi, officiis cum?'},
    ]

    const testPost = db.map(item => item);


    res.render('main', { title: content.title, desc: content.description, post: testPost });
});

module.exports = router;