const express = require('express');
const router = express.Router();


router.get('/', (req, res) => {
    res.send('home page');
})

router.get('/test/bar', (req, res) => {
    res.send('test_bar');
})

router.get('/test/*', (req, res) => {
    res.send('ok');
})


module.exports = router;