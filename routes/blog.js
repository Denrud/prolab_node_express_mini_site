const express = require('express');
const router = express.Router();

router.get('/:id', (req, res) => {
    const { id } = req.params;
    
    const db = [
        { id: '1', title: 'post 1', content: ''},
        { id: '2', title: 'post 3', content: ''},
        { id: '3', title: 'post 3', content: ''}
    ]
    
    const article = db.find(val => val.id === id);
    
    res.render('blog-content',  { title: article.title, content: article.content });
});



module.exports = router;