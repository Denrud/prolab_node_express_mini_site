const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.send('portfolio page');
});

module.exports = router;