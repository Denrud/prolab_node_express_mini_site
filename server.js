const path = require('path');
const express = require('express');
const bodyParser = require('body-parser'); 

const home = require('./routes/home.js');
const portfolio = require('./routes/portfolio');
const contact = require('./routes/contact');
const blog = require('./routes/blog.js');

const server = express();

server.use(express.static(path.join(__dirname, 'public')));
server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'ejs');
/*
server.use(bodyParser.urlencoded({extended: false}));

server.get('/', (req, res) => {
    const html = `<form method="POST" action="/test"> <input type="text" name="test"><button type="submit">send</button></form>`;
    res.send(html);
})

server.post('/test', (req, res) => {
    console.log("body:", req.body);
    res.send('ok');
})
*/

server.use('/', home);
server.use('/portfolio', portfolio);
server.use('/contact', contact);
server.use('/blog', blog)

server.listen(8000);